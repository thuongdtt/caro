matrix = [
    [" "," "," "], 
    [" "," "," "],
    [" "," "," "]
]
symbol = "x"
def print_matrix(matrix):
    #create matrix as sample | |
    for i in range(3):
        row = "|";    
        for j in range(3):     
            row += str(matrix[i][j]) + "|" 
        print(row)
print_matrix(matrix)

def enter_location():
    #get correct location
    inp = input("Enter location x,y (0< x,y <3)[sample: x[space]y ]:  ")
    location = inp.split()
    while (len(location) != 2) or (location[0].isnumeric() is False) or (location[1].isnumeric() is False) or (int(location[0]) > 2) or (int(location[1]) > 2) or matrix[int(location[1])][int(location[0])] != " ":
        print("Wrong location! Please enter again ^^ ")
        inp = input("Enter location x,y (0< x,y <3): ")
        location = inp.split()
    return location

def play_game(matrix,location,symbol):
    #play caro with location 
    matrix[int(location[1])][int(location[0])] = symbol
    return matrix

def end_game(matrix):
    #case 1: end game if matrix is full
    for i in range(3):
        for j in range(3):
            if matrix[i][j] == " ":
                return False
    print("Full slot! Game over! ")
    return True

def exist_winner(matrix):
#case 2: end game if any player win with 3 similar symbols
    #2.1: check columns:
    for i in range(3):
        count_x_column = 0
        count_o_column = 0
        for j in range(3):
            if matrix[j][i] == "x":
                count_x_column += 1
            if matrix[j][i] == "o":
                count_o_column += 1
             #return winner
            if count_x_column == 3:
                return "x"
            if count_o_column == 3:
                return "o"
    #2.2: check rows
    for i in range(3):
        count_x_row = matrix[i].count("x")
        count_o_row = matrix[i].count("o")
    #return winner
        if count_x_row == 3:
            return "x"
        if count_o_row == 3:
            return "o"
    #2.3: check 2 diagonals
    #2.3.1: check one diagonal
    count_x_dia_one = 0
    count_o_dia_one = 0
    for i in range(3):
        if matrix[i][i] == "x":
            count_x_dia_one += 1
        if matrix[i][i] == "o":
            count_o_dia_one += 1
    #return winner
    if count_x_dia_one == 3:
        return "x"
    if count_o_dia_one == 3:
        return "o"
    #2.3.2: check the last diagonal
    count_x_dia_two = 0
    count_o_dia_two = 0
    for i in range(3):
        if matrix[2-i][i] == "x":
            count_x_dia_two += 1
        if matrix[2-i][i] == "o":
            count_o_dia_two += 1
    # return winner
    if count_x_dia_two == 3:
        return "x"
    if count_o_dia_two == 3:
        return "o"
    #return None if no winner
    return None

while not end_game(matrix):
    location = enter_location()
    winner = exist_winner(play_game(matrix,location,symbol))
    #if having winner, break while loop
    if winner != None:
        break
    print_matrix(play_game(matrix,location,symbol))  
    if symbol == "x":
        print("Next turn: o")
        symbol = "o"
    else:
        print("Next turn: x")
        symbol = "x"
#end game caro
if winner != None:
    print("Congratulation!", winner, "win")
    print("Game over!")
